defmodule Escala.CMS.QuestionTest do
  use Escala.DataCase
  import Escala.Factory
  alias Escala.CMS

  setup do
    section = insert(:section)
    {:ok, %{section: section}}
  end

  describe "create_question" do
    test "with valid attributes returns new question", %{section: section} do
      attrs =
        %{
          question_text: "what's your name?",
          question_type: "MULTIPLE_CHOICE",
          section_id: section.id
        }

      assert {:ok, _question} = CMS.create_question(attrs)
    end

    test "requires question_text", %{section: section} do
      attrs =
        %{
          question_type: "MULTIPLE_CHOICE",
          section_id: section.id
        }

      assert {:error, changeset} = CMS.create_question(attrs)
      assert errors_on(changeset).question_text == ["can't be blank"]
    end

    test "requires section_id" do
      attrs =
        %{
          question_text: "what's your name?",
          question_type: "MULTIPLE_CHOICE"
        }

      assert {:error, changeset} = CMS.create_question(attrs)
      assert errors_on(changeset).section_id == ["can't be blank"]
    end

    test "requires valid section_id" do
      attrs =
        %{
          question_text: "what's your name?",
          section_id: 1
        }

      assert {:error, changeset} = CMS.create_question(attrs)
      assert errors_on(changeset).section_id == ["is invalid"]
    end

    test "requires question_type", %{section: section} do
      attrs =
        %{
          question_text: "what's your name?",
          section_id: section.id
        }

      assert {:error, changeset} = CMS.create_question(attrs)
      assert errors_on(changeset).question_type == ["can't be blank"]
    end

    test "requires valid question_type", %{section: section} do
      attrs =
        %{
          question_text: "what's your name?",
          section_id: section.id,
          question_type: "invalid"
        }

      assert {:error, changeset} = CMS.create_question(attrs)
      assert errors_on(changeset).question_type == ["is invalid"]
    end
  end
end

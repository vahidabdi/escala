defmodule Escala.QuestionResolverTest do
  use EscalaWeb.ConnCase
  import Escala.Factory

  @query """
  query GetQuestion($id: ID!) {
    question(id: $id) {
      id
      question_text
    }
  }
  """

  @mutation """
  mutation CreateQuestion($input: QuestionInput!) {
    createQuestion(input: $input) {
      id
      question_text
    }
  }
  """

  @nested_mutation """
  mutation CreateQuestion($input: QuestionInput!) {
    createQuestion(input: $input) {
      id
      question_text
      option_choices {
        option_choice_name
        option_choice_value
      }
    }
  }
  """

  describe "query question" do
    test "returns nil without authorization header" do
      question = insert(:question)

      response =
        build_conn()
        |> graphql_query(
             query: @query,
             variables: %{id: question.id}
           )

      assert response["data"]["question"] == nil
    end
  end

  test "returns question" do
    question = insert(:question)
    user = question.section.survey.user

    response =
      build_conn()
      |> authenticate_user(user)
      |> graphql_query(
           query: @query,
           variables: %{id: question.id}
         )

    assert response["data"]["question"]["id"] == question.id
  end

  describe "question mutation" do
    test "creates with valid info" do
      section = insert(:section)
      user = section.survey.user

      response =
        build_conn()
        |> authenticate_user(user)
        |> graphql_query(
             query: @mutation,
             variables: %{
               input: %{
                 question_text: "What's your name?",
                 question_type: "MULTIPLE_CHOICE",
                 section_id: section.id
               }
             }
           )

      assert response["data"]["createQuestion"]["question_text"] == "What's your name?"
    end

    test "creates with nested option_choice" do
      section = insert(:section)
      user = section.survey.user

      response =
        build_conn()
        |> authenticate_user(user)
        |> graphql_query(
             query: @nested_mutation,
             variables: %{
               input: %{
                 question_text: "favourite editor?",
                 question_type: "MULTIPLE_CHOICE",
                 section_id: section.id,
                 optionChoice: [
                   %{
                     option_choice_name: "emacs",
                     option_choice_value: "emcas"
                   },
                   %{
                     option_choice_name: "vim",
                     option_choice_value: "vim"
                   },
                   %{
                     option_choice_name: "atom",
                     option_choice_value: "atom"
                   }
                 ]
               }
             }
           )

      assert length(response["data"]["createQuestion"]["option_choices"]) == 3
    end
  end
end

defmodule Escala.CMSFactory do
  use ExMachina.Ecto, repo: Escala.Repo

  defmacro __using__(_opts) do
    quote do
      def input_type_factory do
        %Escala.CMS.InputType{
          name: "short_text",
          description: "متنی کوتاه",
          has_option: false,
          allow_multiple_option: false,
        }
      end

      def option_group_factory do
        %Escala.CMS.OptionGroup{
          name: "ٔNever-Always",
        }
      end

      def option_choice_factory do
        %Escala.CMS.OptionChoice{
          name: "good",
          value: "5",
          option_group: build(:option_group)
        }
      end

      def survey_factory do
        %Escala.CMS.Survey{
          name: "sample survey",
          user: build(:account_user)
        }
      end

      def section_factory do
        %Escala.CMS.Section{
          title: "section1",
          survey: build(:survey)
        }
      end

      def question_factory do
        %Escala.CMS.Question{
          question_text: "what's your name",
          answer_required: false,
          section: build(:section),
          input_type: build(:input_type),
        }
      end
    end
  end
end

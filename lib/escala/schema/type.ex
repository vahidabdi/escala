defmodule Escala.Schema.Type do
  @moduledoc """
  Graphql Type
  """
  use Absinthe.Schema.Notation
  use Absinthe.Ecto, repo: Escala.Repo

  @desc "The oauth providers"
  enum :providers do
    value(:google, as: "google", description: "Google")
  end

  @desc "User profile"
  object :user do
    @desc "user id"
    field(:id, :id)

    @desc "user email address"
    field(:email, :string)

    @desc "username"
    field(:username, :string)

    @desc "first_name"
    field(:first_name, :string)

    @desc "last_name"
    field(:last_name, :string)

    @desc "profile picture url"
    field(:picture, :string)

    @desc "list of providers"
    field(:providers, list_of(:providers))
  end

  @desc "User Session"
  object :session do
    @desc "user object"
    field(:user, :user)

    @desc "user token"
    field(:jwt, :string)
  end

  @desc "input type"
  object :input_type do
    @desc "input type id"
    field(:id, :id)

    @desc "name"
    field(:name, :string)

    @desc "description"
    field(:description, :string)

    @desc "has option"
    field(:has_option, :boolean)

    @desc "allow multiple option"
    field(:allow_multiple_option, :boolean)
  end

  @desc "option group"
  object :option_group do
    @desc "id"
    field(:id, :id)

    @desc "name"
    field(:name, :string)

    @desc "user"
    field(:user, :user, resolve: assoc(:user))

    @desc "option choices"
    field(:option_choices, list_of(:option_choice), resolve: assoc(:option_choices))
  end

  @desc "option choice"
  object :option_choice do
    @desc "option choice id"
    field(:id, :id)

    @desc "name"
    field(:name, :string)

    @desc "value"
    field(:value, :string)

    @desc "option group"
    field(:option_group, :option_group, resolve: assoc(:option_group_id))

    @desc "question options"
    field(:question, :question, resolve: assoc(:question_id))
  end

  @desc "question option"
  object :question_option do
    @desc "question option id"
    field(:id, :id)

    @desc "question"
    field(:question, :question, resolve: assoc(:question))

    @desc "option choice"
    field(:option_choice, :option_choice, resolve: assoc(:option_choice))
  end

  @desc "question"
  object :question do
    @desc "question id"
    field(:id, :id)

    @desc "question text"
    field(:question_text, :string)

    @desc "answer required"
    field(:answer_required, :boolean)

    @desc "question position"
    field(:position, :integer)

    @desc "valid"
    field(:valid, :boolean)

    @desc "section"
    field(:section, :section, resolve: assoc(:section))

    @desc "input type"
    field(:input_type, :input_type, resolve: assoc(:input_type))

    @desc "option group"
    field(:option_group, :option_group, resolve: assoc(:option_group))

    @desc "question options"
    field(:question_options, list_of(:question_option), resolve: assoc(:question_options))

  end

  @desc "survey"
  object :survey do
    @desc "survey id"
    field(:id, :id)

    @desc "survey name"
    field(:name, :string)

    @desc "welcome"
    field(:welcome, :string)

    @desc "title"
    field(:title, :string)

    @desc "description"
    field(:description, :string)

    @desc "user"
    field(:user, :user, resolve: assoc(:user))

    @desc "sections"
    field(:sections, list_of(:section), resolve: assoc(:sections))
  end

  @desc "section"
  object :section do
    @desc "section id"
    field(:id, :id)

    @desc "title"
    field(:title, :string)

    @desc "subtitle"
    field(:subtitle, :string)

    @desc "survey"
    field(:survey, :survey, resolve: assoc(:survey))

    @desc "questions"
    field(:questions, list_of(:question), resolve: assoc(:questions))
  end

  # input objects

  @desc "option group input"
  input_object :option_group_input do
    field(:name, non_null(:string))
    field(:option_choices, list_of(:option_choice_input))
  end

  @desc "option choice input"
  input_object :option_choice_input do
    field(:name, non_null(:string))
    field(:value, non_null(:string))
  end

  @desc "question input"
  input_object :question_input do
    field(:question_text, non_null(:string))
    field(:answer_required, :boolean)
    field(:section_id, non_null(:id))
    field(:input_type_id, non_null(:id))
    field(:option_group_id, :id)
  end

  @desc "survey input"
  input_object :survey_input do
    field(:name, non_null(:string))
    field(:welcome, :string)
    field(:title, :string)
    field(:description, :string)
  end

  @desc "section input"
  input_object :section_input do
    field(:title, non_null(:string))
    field(:subtitle, :string)
    field(:survey_id, non_null(:id))
  end
end

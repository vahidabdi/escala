defmodule Escala.CMS.Question do
  @moduledoc """
  Question Module
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Escala.CMS.Question
  alias Escala.CMS.Section
  alias Escala.CMS.InputType
  alias Escala.CMS.OptionGroup
  alias Escala.CMS.QuestionOption

  @primary_key {:id, Ecto.UUID, read_after_writes: true}
  @foreign_key_type Ecto.UUID
  @timestamps_opts [type: :utc_datetime, usec: false]

  schema "questions" do
    field(:question_text, :string)
    field(:answer_required, :boolean, default: false)
    field(:position, :integer, read_after_writes: true)
    field(:valid, :boolean)

    belongs_to(:section, Section)
    belongs_to(:input_type, InputType)
    belongs_to(:option_group, OptionGroup)
    has_many(:question_options, QuestionOption)

    timestamps()
  end

  @doc false
  def changeset(%Question{} = question, attrs) do
    question
    |> cast(attrs, [
         :question_text,
         :answer_required,
         :position,
         :valid,
         :section_id,
         :input_type_id,
         :option_group_id,
       ])
    |> validate_required([:question_text, :section_id, :input_type_id])
    |> foreign_key_constraint(:section_id)
    |> foreign_key_constraint(:input_type_id)
    |> foreign_key_constraint(:option_group_id)
    |> unique_constraint(:position, name: :unique_position)
  end
end

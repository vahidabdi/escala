defmodule Escala.CMS.InputType do
  @moduledoc """
  Option Choice
  """
  use Ecto.Schema
  import Ecto.Changeset
  alias Escala.CMS.InputType
  alias Escala.CMS.Question

  @primary_key {:id, Ecto.UUID, read_after_writes: true}
  @foreign_key_type Ecto.UUID
  @timestamps_opts [type: :utc_datetime, usec: false]

  schema "input_types" do
    field(:name, :string)
    field(:description, :string)
    field(:has_option, :boolean)
    field(:allow_multiple_option, :boolean)

    has_many(:questions, Question)
    timestamps()
  end

  @doc false
  def changeset(%InputType{} = input_type, attrs) do
    input_type
    |> cast(attrs, [:name, :description, :has_option, :allow_multiple_option])
    |> validate_required([:name, :description, :has_option])
  end
end

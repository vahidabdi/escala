defmodule Escala.CMS.OptionGroupResolver do
  alias Escala.CMS
  alias Escala.Accounts

  def list_option_groups(_, %{context: %{current_user: %{id: user_id}}}) do
    {:ok, CMS.list_option_groups(user_id)}
  end

  def list_option_groups(_, _) do
    {:error, "مجوز ندارید"}
  end

  def create(%{input: args}, %{context: %{current_user: %{id: user_id}}}) do
    {option_choice, args} = Map.pop(args, :option_choices)
    user = Accounts.get_user(user_id)

    case user do
      nil ->
        {:error, "مجوز ساخت ندارید"}
      _user ->
        args = Map.merge(args, %{user_id: user_id})

        case CMS.create_option_group(args) do
          {:ok, og} ->
            case option_choice do
              nil ->
                nil
              ocs ->
                Enum.map(ocs, fn oc ->
                  Map.merge(oc, %{option_group_id: og.id})
                  |> CMS.create_option_choice()
                end)
            end
            {:ok, og}
          {:error, _changeset} ->
            {:error, "خطا در ایجاد گروه"}
        end
    end
  end

  def create(_, _) do
    {:error, "مجوز ندارید"}
  end
end

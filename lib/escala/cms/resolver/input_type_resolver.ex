defmodule Escala.CMS.InputTypeResolver do
  alias Escala.CMS

  def list_input_types(_, _) do
    {:ok, CMS.list_input_types()}
  end
end

defmodule Escala.CMS.SectionResolver do
  @moduledoc """
  Section Resolver
  """
  alias Escala.CMS

  def find(%{id: id}, %{context: %{current_user: %{id: _user_id}}}) do
    case CMS.get_section(id) do
      nil ->
        {:error, "survey not found"}

      section ->
        {:ok, section}
    end
  end

  def find(_, _) do
    {:error, "مجوز ندارید"}
  end

  def create_section(%{input: args}, %{context: %{current_user: %{id: _user_id}}}) do
    case CMS.create_section(args) do
      {:ok, section} -> {:ok, section}
      {:error, _changeset} -> {:error, "خطا در ایجاد قسمت"}
    end
  end

  def create_section(_, _) do
    {:error, "محوز ساخت قسمت دارید"}
  end

  # def update_survey(%{id: id, input: args}, %{context: %{current_user: %{id: _id}}}) do
  #   survey = CMS.get_survey(id)
  #
  #   case survey do
  #     nil ->
  #       {:error, "مطالعه یافت نشد"}
  #
  #     s ->
  #       case CMS.update_survey(s, args) do
  #         {:ok, u_survey} -> {:ok, u_survey}
  #         {:error, _changeset} -> {:error, "خطا در بروز رسانی مطالعه"}
  #       end
  #   end
  # end
  #
  # def update_survey(_, _) do
  #   {:error, "مجوز بروز رسانی مطالعه را ندارید"}
  # end
end

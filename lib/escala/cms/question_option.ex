defmodule Escala.CMS.QuestionOption do
  @moduledoc """
  Question Option
  """

  use Ecto.Schema
  import Ecto.Changeset
  alias Escala.CMS.QuestionOption
  alias Escala.CMS.Question
  alias Escala.CMS.OptionChoice

  @primary_key {:id, Ecto.UUID, read_after_writes: true}
  @foreign_key_type Ecto.UUID

  schema "question_options" do
    belongs_to(:question, Question)
    belongs_to(:option_choice, OptionChoice)
  end

  @doc false
  def changeset(%QuestionOption{} = question_option, attrs) do
    question_option
    |> cast(attrs, [:question_id, :option_choice_id])
    |> validate_required([:question_id, :option_choice_id])
    |> foreign_key_constraint(:question_id)
    |> foreign_key_constraint(:option_choice_id)
  end
end

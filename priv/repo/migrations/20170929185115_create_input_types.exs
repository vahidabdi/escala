defmodule Escala.Repo.Migrations.CreateInputType do
  use Ecto.Migration

  def change do
    create table(:input_types, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :name, :text, null: false
      add :description, :text, null: false
      add :has_option, :boolean, null: false, default: false
      add :allow_multiple_option, :boolean, null: false, default: false

      timestamps(type: :timestamptz)
    end
  end
end

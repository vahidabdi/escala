defmodule Escala.Repo.Migrations.CreateSection do
  use Ecto.Migration

  def change do
    create table(:sections, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :survey_id, references(:surveys, on_delete: :delete_all, type: :uuid), null: false
      add :title, :text, null: false
      add :subtitle, :text

      timestamps(type: :timestamptz)
    end

    create index(:sections, [:survey_id])
  end
end

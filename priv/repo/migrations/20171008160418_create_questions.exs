defmodule Escala.Repo.Migrations.CreateQuestions do
  use Ecto.Migration

  def change do
    create table(:questions, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :question_text, :text, null: false
      add :answer_required, :boolean, default: false, null: false
      add :position, :integer, null: false
      add :valid, :boolean, null: false, default: false
      add :section_id, references(:sections, on_delete: :delete_all, type: :uuid), null: false
      add :input_type_id, references(:input_types, on_delete: :delete_all, type: :uuid), null: false
      add :option_group_id, references(:option_groups, on_delete: :delete_all, type: :uuid)

      timestamps(type: :timestamptz)
    end

    create constraint(:questions, :position_must_be_positive, check: "position > 0")
    create index(:questions, [:section_id])
    create index(:questions, [:input_type_id])
    create index(:questions, [:option_group_id])
    create unique_index(:questions, [:section_id, :position], name: :unique_position)
  end
end

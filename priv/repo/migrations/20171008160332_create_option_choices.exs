defmodule Escala.Repo.Migrations.CreateOptionChoices do
  use Ecto.Migration

  def change do
    create table(:option_choices, primary_key: false) do
      add :id, :uuid, primary_key: true, default: fragment("uuid_generate_v4()")
      add :name, :text, null: false
      add :value, :text, null: false
      add :option_group_id, references(:option_groups, on_delete: :delete_all, type: :uuid), null: false

      timestamps(type: :timestamptz)
    end

    create index(:option_choices, [:option_group_id])
  end
end

defmodule Escala.Repo.Migrations.GeneratePosition do
  use Ecto.Migration

  def up do
    execute """
    CREATE OR REPLACE FUNCTION update_question_position()
    RETURNS trigger AS $$
    BEGIN
      NEW.position = COALESCE(
        (SELECT max(position) from questions where section_id=NEW.section_id) + 1,
        1);
      RETURN NEW;
    END;
    $$ LANGUAGE plpgsql;
    """

    execute """
    DROP TRIGGER IF EXISTS set_position_trg ON questions;
    """

    execute """
    CREATE TRIGGER set_position_trg
    BEFORE INSERT
    ON questions
    FOR EACH ROW
    EXECUTE PROCEDURE update_question_position();
    """
  end

  def down do
    execute "DROP TRIGGER IF EXISTS set_position_trg ON questions;"
    execute "DROP FUNCTION update_question_position();"
  end
end

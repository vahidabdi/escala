# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Escala.Repo.insert!(%Escala.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Escala.CMS

{:ok, _short_text} = CMS.create_input_type(%{
  name: "short_text",
  description: "متنی کوتاه",
  has_option: false,
  allow_multiple_option: false
  })

{:ok, _long_text} = CMS.create_input_type(%{
  name: "long_text",
  description: "متنی بلند",
  has_option: false,
  allow_multiple_option: false
  })

{:ok, _single_choice} = CMS.create_input_type(%{
  name: "single_choice",
  description: "تک انتخابی",
  has_option: true,
  allow_multiple_option: false
  })

{:ok, _multi_choice} = CMS.create_input_type(%{
  name: "multi_choice",
  description: "چند انتخابی",
  has_option: true,
  allow_multiple_option: true
  })
